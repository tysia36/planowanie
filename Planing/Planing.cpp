// Planing.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#define _USE_MATH_DEFINES
#include <cmath>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>

const int N = 1000;


using namespace std;

class Miasto
{
public:
	Miasto()
	{
		x = 200.0;
		y = 200.0;
		id = 0;
		miasto = "";
	}
	Miasto(double i, double j, int idd, string m)
	{
		x = i;
		y = j;
		id = idd;
		miasto = m;
	}
	void Set_x(double i)
	{
		x = i;
	}
	double Get_x()
	{
		return x;
	}
	void Set_y(double j)
	{
		y = j;
	}
	double Get_y()
	{
		return y;
	}
	void Set_id(int idd)
	{
		id = idd;
	}
	int Get_id()
	{
		return id;
	}
	void Set_miasto(string m)
	{
		miasto = m;
	}
	string Get_miasto()
	{
		return miasto;
	}

private:
	double x;
	double y;
	string miasto;
	int id;
};
class Miasta
{
public:
	Miasta()
	{
	}
	Miasta(int i, Miasto *j)
	{
		n = i;
		i = 0;
		while (i < n)
		{
			osobnik[i] = *(j + i);
			i++;
		}
	}
	void Set_n(double i)
	{
		n = (int)i;
	}
	void Set_osobnik(Miasto *j)
	{
		int i = 0;
		while (i < n)
		{
			osobnik[i] = *(j + i);
			i++;
		}
	}
	double Get_n()
	{
		return n;
	}
	Miasto Get_osobnik(int i)
	{
		if (i < N)
			return osobnik[i];
		else
			return osobnik[N-1];
	}

private:
	int n;
	Miasto osobnik[N];
};

double odleglosc(double x1, double x2, double y1, double y2);
double ocena(Miasta m, int *pokolenie, int n);
int losowanie(int zakres);
int prawidlowyGPS(Miasto one);
void kopiuj(int *one, int *two, int n);
void pokolenie_one(int *p, int n);
void krzyzowanie(int *pokolenieA, int *pokolenieB, int *przydzial, int n);
void mutacja(Miasta m, int *pokolenieB, int n);
void mutacja2(Miasta m, int *pokolenieB, int *najlepszy, int n);
void przepisz(int *pokolenieA, int *pokolenieB, int n);
Miasta wczytaj(int *n);

int main()
{
	int n = 1000; //trzeba wczyta�
	Miasta miast; //trzeba wczyta�

	srand((unsigned int)time(NULL));

	cout << "Wczytano:" << endl;
	miast = wczytaj(&n);

	int *pokolenieA = new int[20 * n];
	int *pokolenieB = new int[20 * n];
	int *najlepszy = new int[n];
	int i = 0;
	int los = 0;
	int o = 0;
	int w, ww;
	double oceny[20];
	int przydzial[20];
	double sum = 0;
	int przy_sum = 0;
	int najw = 0;

	

	
	//cout << odleglosc(miast.Get_osobnik(1).Get_x(), miast.Get_osobnik(99).Get_x(), miast.Get_osobnik(1).Get_y(), miast.Get_osobnik(99).Get_y())<< endl;
	//cout << odleglosc(miast.Get_osobnik(1).Get_x(), miast.Get_osobnik(7).Get_x(), miast.Get_osobnik(1).Get_y(), miast.Get_osobnik(7).Get_y()) << endl;
	pokolenie_one(pokolenieA, n);
	
	kopiuj(najlepszy, pokolenieA, n);
	o = ocena(miast, najlepszy, n);
	
	while (i < 25001)
	{
		sum = 0;
		
		if (i % 50 == 0)
		{
			cout << "Pokolenie nr " << i << endl;
			w = 0;
			while (w < n)
			{
				if (w == (n - 1))
					cout << *(najlepszy + w);
				else
					cout << *(najlepszy + w) << "-";
				w++;
			}
			cout << endl;
			cout << "Ocena: " << ocena(miast, najlepszy, n) << endl;
		}
		/*
		cout << "Pokolenie nr " << i << endl;
		
		ww = 0;
		while (ww < 20)
		{
			cout << "osobnik" << ww << endl;
			w = 0;
			while (w < n)
			{	
				if (w == (n - 1))
					cout << *(pokolenieA + ww*n + w);
				else
					cout << *(pokolenieA + ww*n + w) << "-";
				w++;
			}
			cout << endl<< ocena(miast, pokolenieA + (ww * n), n)<<endl;
			ww++;
		}
		cout << endl;
		*/
		int j = 0;
		while (j < 20)
		{
			oceny[j] = ocena(miast, pokolenieA + (j * n), n);
			if (o > oceny[j])
			{
				kopiuj(najlepszy, pokolenieA + (j * n), n);
				o = ocena(miast, najlepszy, n);
			}
			sum += oceny[j];
			j++;
		}
		j = 0;
		przy_sum = 0;
		najw = 0;
		while (j < 20)
		{
			
			przydzial[j] = round((oceny[j] * 20.0) / sum);
			przy_sum += przydzial[j];
			if (przydzial[j] > przydzial[najw])
				najw = j;
			j++;
		}
		/*if (i == 1000 || i == 2000)
			cout << przy_sum << " " <<najw << " " <<przydzial[0] <<endl;*/
		if (przy_sum < 20)
			przydzial[najw] += 20 - przy_sum;
		else if (przy_sum > 20)
			przydzial[najw] -= przy_sum - 20;
		
		    //cout << "krzyzowanie ";
			krzyzowanie(pokolenieA, pokolenieB, przydzial, n);
			//cout << "aaaaaaaaa " << *(pokolenieB +  n + 25) << " " << *(pokolenieB + n + 30) << " " << *(pokolenieB + n + 35) << " " << *(pokolenieB + n + 40) << endl;
			//cout << "mutacja ";
			mutacja(miast, pokolenieB, n);
			//cout << "przepisanie"<< endl;
			mutacja2(miast, pokolenieB, najlepszy, n);
			przepisz(pokolenieA, pokolenieB, n);
		i++;
	}
	
	
	

	cout << "Ostatnie pokolenie nr " << i << endl;

	ww = 0;
	while (ww < 20)
	{
		cout << "osobnik" << ww << endl;
		w = 0;
		while (w < n)
		{
			if (w == (n - 1))
				cout << *(pokolenieA + ww*n + w);
			else
				cout << *(pokolenieA + ww*n + w) << "-";
			w++;
		}
		cout << endl << "Ocena: " << ocena(miast, pokolenieA + (ww * n), n) << endl;
		ww++;
	}
	cout << endl;

	cout << "Najlepsza trasa:" << endl;
	i = 0;
	while (i < n)
	{
		if (i == (n - 1))
			cout << *(najlepszy + i);
		else
			cout << *(najlepszy + i) << "-";
		i++;
	}
	cout << endl;
	i = 0;
	while (i < n)
	{
		if (i == (n - 1))
			cout << miast.Get_osobnik(*(najlepszy + i)).Get_miasto();
		else
			cout << miast.Get_osobnik(*(najlepszy + i)).Get_miasto() << "-";
		i++;
	}
	cout << endl;
	cout << "Ocena: " << ocena(miast, najlepszy, n) << endl;

	getchar();
    return 0;
}

double odleglosc(double x1, double x2, double y1, double y2)
{
	double r = 0;
	//r = sqrt(pow((x2-x1),2.0)+pow((cos((x1*M_PI)/180)*(y2-y1)),2.0))*(40075.704/360);
	x1 *= M_PI/180;
	x2 *= M_PI/180;
	y1 *= M_PI/180;
	y2 *= M_PI/180;
	//cout << asin(1)*180 / M_PI <<endl;
	
	r = sin(x1)*sin(x2)+cos(x1)*cos(x2)*cos(y1-y2);
	r = acos(r) * 180 / M_PI;
	r = 6371 * r * M_PI / 180;
	
	return r;
}

double ocena(Miasta m, int *pokolenie, int n)
{
	int i = 0;
	double o = 0;
	Miasto m1;
	Miasto m2;

	while (i < n)
	{
		if (prawidlowyGPS(m.Get_osobnik(*(pokolenie+i))) == 0)
		{
			o = 0;
			break;
		}
		if (i == 0)
			i++;
		else
		{
			m1 = m.Get_osobnik(*(pokolenie + i - 1));
			m2 = m.Get_osobnik(*(pokolenie + i));
			o += odleglosc(m1.Get_x(), m2.Get_x(), m1.Get_y(), m2.Get_y());
			i++;
		}
	}

	return o;
}

int losowanie(int zakres)
{
	
	
	return (rand() % zakres);
}

int prawidlowyGPS(Miasto one)
{
	if ((one.Get_x() != 200.0) && (one.Get_y() != 200.0))
		return 1;
	else
		return 0;
}

void kopiuj(int *one, int *two, int n)
{
	int i = 0;
	while (i < n)
	{
		*(one + i) = *(two + i);
		i++;
	}
}
void pokolenie_one(int *p, int n)
{
	int i = 0;
	int j = 0;
	int z = 0;
	int switched = 0;
	while (i < 20)
	{
		switch (i)
		{
		case 0:	//od 0 do n-1
			j = 0;
			while (j < n)
			{

				*(p + (i*n) + j) = j;
					j++;
			}
			break;
		case 1:	//od n-1 do 0
			j = 0;
			while (j < n)
			{

				*(p + (i*n) + j) = n - j - 1;
				j++;
			}
			break;
		case 2: //od 0 do n-1, ale zamieniony pierwszy z ostatnim
			j = 0;
			while (j < n)
			{
				if (j == 0)
					*(p + (i*n) + j) = n - 1;
				else if (j == n - 1)
					*(p + (i*n) + j) = 0;
				else
					*(p + (i*n) + j) = j;
				j++;
			}
			break;
		case 3:  //od n-1 do 0, ale zamieniony pierwszy z ostatnim
			j = 0;
			while (j < n)
			{
				if (j == 0)
					*(p + (i*n) + j) = 0;
				else if (j == n - 1)
					*(p + (i*n) + j) = n - 1;
				else
					*(p + (i*n) + j) = n - j - 1;
				j++;
			}
			break;
		case 4: //od 0 do n-1, ale zamieniony pierwszy z drugim
			j = 0;
			while (j < n)
			{
				if (j == 0)
					*(p + (i*n) + j) = 1;
				else if (j == 1)
					*(p + (i*n) + j) = 0;
				else
					*(p + (i*n) + j) = j;
				j++;
			}
			break;
		case 5: //od n-1 do 0, ale zamieniony ostatni z przedostatnim
			j = 0;
			while (j < n)
			{
				if (j == n - 1)
					*(p + (i*n) + j) = n - 2;
				else if (j == n - 2)
					*(p + (i*n) + j) = n - 1;
				else
					*(p + (i*n) + j) = n - j - 1;
				j++;
			}
			break;
		case 6: // najpierw parzyste, potem nieparzyste, kolejno�� rosn�ca
			j = 0;
			z = 0;
			while (j < n)
			{
				if (z < n)
					*(p + (i*n) + j) = z;
				else
				{
					z = 1;
					*(p + (i*n) + j) = z;
				}
				j++;
				z += 2;
			}
			break;
		case 7: // najpierw parzyste, potem nieparzyste lub odwrotnie (zale�y od parzysto�ci n), ale kolejno�� malej�ca
			j = 0;
			z = n - 1;
			while (j < n)
			{
				if (z >= 0)
					*(p + (i*n) + j) = z;
				else
				{
					z = n - 2;
					*(p + (i*n) + j) = z;
				}
				j++;
				z -= 2;
			}
			break;
		case 8: // najpierw nieparzyste, potem parzyste, kolejno�� rosn�ca
			j = 0;
			z = 1;
			while (j < n)
			{
				if (z < n)
					*(p + (i*n) + j) = z;
				else
				{
					z = 0;
					*(p + (i*n) + j) = z;
				}
				j++;
				z += 2;
			}
			break;
		case 9: //najpierw parzyste, potem nieparzyste lub odwrotnie - odwrotnie ni� w case 7 (zale�y od parzysto�ci n), ale kolejno�� malej�ca
			j = 0;
			z = n - 2;
			while (j < n)
			{
				if (z >= 0)
					*(p + (i*n) + j) = z;
				else
				{
					z = n - 1;
					*(p + (i*n) + j) = z;
				}
				j++;
				z -= 2;
			}
			break;
		case 10: // permutacja co trzeciego elementu
			j = 0;
			z = 0;
			switched = 0;
			while (j < n)
			{
				if (z < n)
					*(p + (i*n) + j) = z;
				else if (switched == 0)
				{
					z = 1;
					*(p + (i*n) + j) = z;
					switched = 1;
				}
				else
				{
					z = 2;
					*(p + (i*n) + j) = z;
				}
				j++;
				z += 3;
			}
			break;
		case 11: // permutacja co trzeciego elementu
			j = 0;
			z = 0;
			switched = 0;
			while (j < n)
			{
				if (z < n)
					*(p + (i*n) + j) = z;
				else if (switched == 0)
				{
					z = 2;
					*(p + (i*n) + j) = z;
					switched = 1;
				}
				else
				{
					z = 1;
					*(p + (i*n) + j) = z;
				}
				j++;
				z += 3;
			}
			break;
		case 12: // permutacja co trzeciego elementu
			j = 0;
			z = 1;
			switched = 0;
			while (j < n)
			{
				if (z < n)
					*(p + (i*n) + j) = z;
				else if (switched == 0)
				{
					z = 0;
					*(p + (i*n) + j) = z;
					switched = 1;
				}
				else
				{
					z = 2;
					*(p + (i*n) + j) = z;
				}
				j++;
				z += 3;
			}
			break;
		case 13: // permutacja co trzeciego elementu
			j = 0;
			z = 1;
			switched = 0;
			while (j < n)
			{
				if (z < n)
					*(p + (i*n) + j) = z;
				else if (switched == 0)
				{
					z = 2;
					*(p + (i*n) + j) = z;
					switched = 1;
				}
				else
				{
					z = 0;
					*(p + (i*n) + j) = z;
				}
				j++;
				z += 3;
			}
			break;
		case 14: // permutacja co trzeciego elementu
			j = 0;
			z = 2;
			switched = 0;
			while (j < n)
			{
				if (z < n)
					*(p + (i*n) + j) = z;
				else if (switched == 0)
				{
					z = 0;
					*(p + (i*n) + j) = z;
					switched = 1;
				}
				else
				{
					z = 1;
					*(p + (i*n) + j) = z;
				}
				j++;
				z += 3;
			}
			break;
		case 15: // permutacja co trzeciego elementu
			j = 0;
			z = 2;
			switched = 0;
			while (j < n)
			{
				if (z < n)
					*(p + (i*n) + j) = z;
				else if (switched == 0)
				{
					z = 1;
					*(p + (i*n) + j) = z;
					switched = 1;
				}
				else
				{
					z = 0;
					*(p + (i*n) + j) = z;
				}
				j++;
				z += 3;
			}
			break;
		case 16: // permutacja co trzeciego elementu - zamienione 2 ostatnie
			j = 0;
			z = 0;
			switched = 0;
			while (j < n)
			{
				if (z < n)
					*(p + (i*n) + j) = z;
				else if (switched == 0)
				{
					z = 1;
					*(p + (i*n) + j) = z;
					switched = 1;
				}
				else
				{
					z = 2;
					*(p + (i*n) + j) = z;
				}
				j++;
				z += 3;
			}
			z = *(p + (i*n) + n - 2);
			*(p + (i*n) + n - 2) = *(p + (i*n) + n - 1);
			*(p + (i*n) + n - 1) = z;
			break;
		case 17: // permutacja co trzeciego elementu - zamienione 2 pierwsze
			j = 0;
			z = 0;
			switched = 0;
			while (j < n)
			{
				if (z < n)
					*(p + (i*n) + j) = z;
				else if (switched == 0)
				{
					z = 1;
					*(p + (i*n) + j) = z;
					switched = 1;
				}
				else
				{
					z = 2;
					*(p + (i*n) + j) = z;
				}
				j++;
				z += 3;
			}
			z = *(p + (i*n));
			*(p + (i*n)) = *(p + (i*n) + 1);
			*(p + (i*n) + 1) = z;
			break;
		case 18: // permutacja co trzeciego elementu - zamieniony pierwszy z ostatnim
			j = 0;
			z = 0;
			switched = 0;
			while (j < n)
			{
				if (z < n)
					*(p + (i*n) + j) = z;
				else if (switched == 0)
				{
					z = 1;
					*(p + (i*n) + j) = z;
					switched = 1;
				}
				else
				{
					z = 2;
					*(p + (i*n) + j) = z;
				}
				j++;
				z += 3;
			}
			z = *(p + (i*n));
			*(p + (i*n)) = *(p + (i*n) + n - 1);
			*(p + (i*n) + n - 1) = z;
			break;
		case 19: // najpierw parzyste, potem nieparzyste, kolejno�� rosn�ca - zamienione dwa pierwsze
			j = 0;
			z = 0;
			while (j < n)
			{
				if (z < n)
					*(p + (i*n) + j) = z;
				else
				{
					z = 1;
					*(p + (i*n) + j) = z;
				}
				j++;
				z += 2;
			}
			z = *(p + (i*n));
			*(p + (i*n)) = *(p + (i*n) + 1);
			*(p + (i*n) + 1) = z;
			break;
		default:
			break;
		}
		i++;
	}
}

void krzyzowanie(int *pokolenieA, int *pokolenieB, int *przydzial, int n)
{
	int i = 0;
	int j = 0;
	int k = 0;
	int zakres = n / 4;
	int los = losowanie(2 * zakres); //2*zakres //3*zakres
	int los2 = losowanie(zakres) + zakres;    // +zakres po losowaniu //los(zakres)
	//cout << "wylosowany przedzial"<<los << " " << los2 << endl;
	int najw = 0;
	int nast = 0;
	int pom = 0; 

	while (i < 10)
	{
		j = 0;
		najw = 0;
		nast = 0;

		while (j < 20)
		{
			if (*(przydzial + najw) < *(przydzial + j))
				najw = j;
			j++;
		}

		j = 0;
		while (j < 20)
		{
			if (*(przydzial + nast) < *(przydzial + j) && j != najw)
				nast = j;
			else if (*(przydzial + nast) <= *(przydzial + j) && nast == najw)
				nast = j;
			j++;
		}
		//cout << "wybrane osobniki" << najw << " " << nast << endl;
		(*(przydzial + nast))--;
		(*(przydzial + najw))--;

		k = 0;
		while (k < n)
		{
			*(pokolenieB + (2 * i * n) + k) = *(pokolenieA + (najw * n) + k);
			*(pokolenieB + (2 * i * n) + n + k) = *(pokolenieA + (nast * n) + k);
			k++;
		}
		k = los;
		while (k < (los + los2))
		{
			
			//cout << "zzzzzzzzzzzzzz" << *(pokolenieB + (2 * i * n) + k) << " " << *(pokolenieB + (2 * i * n) + n + k) << " ";
			j = 0;
			while (j < n)
			{
				if (*(pokolenieA + (nast * n) + k) == *(pokolenieB + (2 * i * n) + j) && (j != k))
					*(pokolenieB + (2 * i * n) + j) = *(pokolenieB + (2 * i * n) + k);
				if (*(pokolenieA + (najw * n) + k) == *(pokolenieB + (2 * i * n) + n + j) && (j != k))
					*(pokolenieB + (2 * i * n) + n + j) = *(pokolenieB + (2 * i * n) + n + k);
				j++;
			}
			*(pokolenieB + (2 * i * n) + k) = *(pokolenieA + (nast * n) + k);
			*(pokolenieB + (2 * i * n) + n + k) = *(pokolenieA + (najw * n) + k);
			//cout << "pppppppppp" << *(pokolenieB + (2 * i * n) + k) << " " << *(pokolenieB + (2 * i * n) + n + k) << " "<<endl;
			k++;
		}
		i++;
	}
}
void mutacja(Miasta m, int *pokolenieB, int n)
{
	int los = 0;
	int i = 0;
	int j = 0;
	int k = 0;
	int pom = 0;
	int odl = 0;
	Miasto m1, m2;
	while (i < 6)
	{
		j = losowanie(n-1);
		los = losowanie(20);
		m1 = m.Get_osobnik(*(pokolenieB + (los * n) +j));
		k = 0;
		pom = 0;
		while (k < n)
		{
			m2 = m.Get_osobnik(*(pokolenieB +(los * i) + k));
			if (k == 0)
			{
				odl = odleglosc(m1.Get_x(),m2.Get_x(),m1.Get_y(),m2.Get_y());
			}
			else if (odleglosc(m1.Get_x(), m2.Get_x(), m1.Get_y(), m2.Get_y()) < odl)
			{
				pom = k;
				odl = odleglosc(m1.Get_x(), m2.Get_x(), m1.Get_y(), m2.Get_y());
			}
			k++;
		}
		k = pom;
		//cout << j << " " << k << " " << los << endl;
		pom = *(pokolenieB + (los * n) + j + 1);
		*(pokolenieB + (los * n) + j + 1) = *(pokolenieB + (los * n) + k);
		*(pokolenieB + (los * n) + k) = pom;
	//	cout << "zzzzzzzzzzzzzzzz" << *(pokolenieB + (los * n) + j + 1) << " " << *(pokolenieB + (los * n) + k) << endl;
		i++;
	}
}
void mutacja2(Miasta m, int *pokolenieB, int *najlepszy, int n)
{
	int i = 0;
	double o = 0;
	double najg = 0;
	while (i < 20)
	{
		if (i == 0)
			o = ocena(m, pokolenieB, n);
		else if (ocena(m, pokolenieB + i*n, n) > o)
		{
			o = ocena(m, pokolenieB + i*n, n);
			najg = i;
		}
		i++;
	}
	kopiuj(pokolenieB + (i-1)*n,najlepszy,n);
}

void przepisz(int *pokolenieA, int *pokolenieB, int n)
{
	int i = 0;
	int j = 0;
	while (i < 20)
	{
		j = 0;
		while (j < n)
		{
		//	cout << "efefefe" << *(pokolenieA + (i * n) + j) << " " << *(pokolenieB + (i * n) + j) << " ";
			*(pokolenieA + (i * n) + j) = *(pokolenieB + (i * n) + j);
			//cout << "efefefe"<< *(pokolenieA + (i * n) + j) <<" "<<*(pokolenieB + (i * n) + j)<<endl;
			j++;
		}
		i++;
	}
}

Miasta wczytaj(int *n)
{
	Miasto *mi;
	Miasta miasto;
	double x, y;
	int i;
	string m;
	ifstream myfile("dane.txt");
	if (myfile.is_open())
	{
		myfile >> *n;
		i = 0;
		cout << *n << endl;
		mi = new Miasto[*n];
		while (i < *n)
		{
			myfile >> m;
			cout << m<<" ";
			(*(mi + i)).Set_miasto(m);
			myfile >> x;
			cout << x<< " ";
			(*(mi + i)).Set_x(x);
			myfile >> y;
			cout << y << endl;
			(*(mi + i)).Set_y(y);
			i++;
		}
		myfile.close();
		Miasta miast(*n, mi);
		return miast;
	}
	else cout << "Unable to open file";

	return miasto;
}